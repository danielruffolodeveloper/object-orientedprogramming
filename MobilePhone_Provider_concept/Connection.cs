﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication9
{
    class Connection
    {
        private Customer _customer = null;

        private Plans _plan = null;

        private Device _device = null;

        private string _deviceNumber = "INVALID";

        public Customer Customer
        {
            get { return _customer; }
        }

        public Plans Plan
        {
            get { return _plan; }
        }

        public Device Device
        {
            get { return _device; }
        }

        public string _DeviceNumber
        {
            get { return _deviceNumber; }
        }

         public Connection(Customer Customer, Device Device, Plans Plan)
        {
             
            if (SystemDB.Customers.Contains(Customer))
            {
                if (Customer.ApplyCharge(Device.devPrice(Device)) == true && Customer.ApplyCharge(Plan.PlanPrice(Plan))==true)

                {
                    _customer = Customer;
                    _device = Device;
                    _plan = Plan;

                    _deviceNumber = Utility.AllocatePhoneNumber();
                    SystemDB.Register(this);
                    Customer.Register(this);
                }
               
            }
                else
            {
                return;
            }

            
        }
         public override string ToString()
         {
             return string.Format("{0}:{1}\n\t\t\t{2}",_deviceNumber,_device, _plan);
         }
    }
}
