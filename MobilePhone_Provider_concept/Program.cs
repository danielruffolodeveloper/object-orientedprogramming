﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication9
{
    class Program
    {
        private static void LoadDefaultData()
        {
            //////////////////////////////////////////////////////////////////////
            // Create and Register Phones
            VoiceDevice ssE1205 = new VoiceDevice("Samsung", "Keystone E1205");
            VoiceDevice ssC3520 = new VoiceDevice("Samsung", "C3520 Flip");
            DataDevice aplIPadAir = new DataDevice("Apple", "iPad Air 2 128GB");
            DataDevice aplIPadMini = new DataDevice("Apple", "iPad Mini 3 128GB");
            SmartPhone aplIPhone6 = new SmartPhone("Apple", "iPhone 6 128GB");
            SmartPhone aplIPhone6Plus = new SmartPhone("Apple", "iPhone 6 Plus 128GB");
            SmartPhone ssGalaxy = new SmartPhone("Samsung", "Galaxy S6 128GB");
            SmartPhone ssGalaxyEdge = new SmartPhone("Samsung", "Galaxy S6 Edge 128GB");
            //inventory processor
            SystemDB.Register(new DeviceInventory(ssE1205, 55.00M));
            SystemDB.Register(new DeviceInventory(ssC3520, 89.00M));
            SystemDB.Register(new DeviceInventory(aplIPadAir, 1019.00M));
            SystemDB.Register(new DeviceInventory(aplIPadMini, 899.00M));
            SystemDB.Register(new DeviceInventory(aplIPhone6, 1299.00M));
            SystemDB.Register(new DeviceInventory(aplIPhone6Plus, 1449.00M));
            SystemDB.Register(new DeviceInventory(ssGalaxy, 1299.00M));
            SystemDB.Register(new DeviceInventory(ssGalaxyEdge, 1449.00M));

            //////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////
            // Create and Register plans
            VoicePlans Chatterbox = new VoicePlans("Chatterbox","$30 of calls");
            VoicePlans Conversationalist = new VoicePlans("Conversationalist", "60 of calls");
            VoicePlans Socialite = new VoicePlans("Socialite", "100 of calls");

            DataPlans Beginner = new DataPlans("Beginner", "");
            DataPlans Casual = new DataPlans("Casual", "");
            DataPlans Addict = new DataPlans("Addict", "");

            ComboPlans Quickie = new ComboPlans("Quickie", "");
            ComboPlans Nimble = new ComboPlans("Nimble", "");
            ComboPlans Connected = new ComboPlans("Connected", "");
            //inventory processor
            SystemDB.RegisterPlan(new PlanInventory(Chatterbox, 30.00M));
            SystemDB.RegisterPlan(new PlanInventory(Conversationalist, 50.00M));
            SystemDB.RegisterPlan(new PlanInventory(Socialite, 80.00M));

            SystemDB.RegisterPlan(new PlanInventory(Beginner, 10.00M));
            SystemDB.RegisterPlan(new PlanInventory(Casual, 20.00M));
            SystemDB.RegisterPlan(new PlanInventory(Addict, 30.00M));

            SystemDB.RegisterPlan(new PlanInventory(Quickie, 50.00M));
            SystemDB.RegisterPlan(new PlanInventory(Nimble, 80.00M));
            SystemDB.RegisterPlan(new PlanInventory(Connected, 100.00M));

            //////////////////////////////////////////////////////////////////////

            // Create and Register Customers
            Customer fred = new Customer("Fred", 4500.0M);
            Customer wilma = new Customer("Wilma", 2500.0M);
            Customer barney = new Customer("Barney", 1500.0M);
            Customer betty = new Customer("Betty", 500.0M);

            SystemDB.Register(fred);
            SystemDB.Register(wilma);
            SystemDB.Register(barney);
            SystemDB.Register(betty);

           

            // Register phone connections
            new Connection(fred, ssE1205, Chatterbox);
            new Connection(fred, aplIPadAir, Beginner);
            new Connection(fred, aplIPhone6,Quickie);
            new Connection(wilma, ssC3520,Conversationalist);
            new Connection(wilma, aplIPadMini,Casual);
            new Connection(wilma, aplIPhone6Plus,Nimble);
            new Connection(barney, ssE1205,Socialite);
            new Connection(barney, aplIPadAir,Addict);
            new Connection(barney, ssGalaxy,Connected);
            new Connection(betty, ssC3520,Chatterbox);
            new Connection(betty, aplIPadMini,Beginner);
            new Connection(betty, ssGalaxyEdge,Quickie);
        }

        static void Main(string[] args)
        {
            LoadDefaultData();

            Console.WriteLine("Customers:");
            SystemDB.DumpCustomers(Console.Out, "\t");
            Console.WriteLine();

            Console.WriteLine("Connections:");
            SystemDB.DumpConnections(Console.Out, "\t");
            Console.WriteLine();
        }
    }
}
