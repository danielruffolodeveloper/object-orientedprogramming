﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication9
{
    class Plans
    {
        
        //attributes
        private string _Plan;
        private string _PlanInfo;

        
        public string Plan
        {
            get
            {
                return _Plan;
            }
        }

        public string planInfo
        {
            get
            {
                return _PlanInfo;
            }
        }

            
        protected  Plans(string Plan,string PlanInfo)
        {
            _Plan = Plan;
            _PlanInfo = PlanInfo;

        }

        public decimal PlanPrice(Plans plan)
        {
            decimal PlanPrice = 0;
            foreach (PlanInventory price in SystemDB.Plans)
            {
                if (price.Plan == plan)
                {
                    PlanPrice = price.Price;

                }

            }
            return PlanPrice;
        }

        public override string ToString()
        {
            return string.Format("{0}, {1}", _Plan ,_PlanInfo);
        }
    }
}
