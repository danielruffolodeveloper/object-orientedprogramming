﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication9
{
    class DataPlans : Plans
    {
        public DataPlans(string Plan, string PlanInfo): base(Plan, PlanInfo)
        {
        }
        public override string ToString()
        {
            return string.Format("{0} {1}", base.ToString(), "(Data Plan)");
        }

    }
}