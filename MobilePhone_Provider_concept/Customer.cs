﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace ConsoleApplication9
{
    class Customer
    {
        private string _Name;
        private decimal _Balance;

        private List<Connection>_Connections = new List<Connection>();

        public string Name

        {
            get { return _Name; }
        }

        public decimal Balance
        {
            get { return _Balance; }
        }
        public ReadOnlyCollection<Connection> Connections
        {
            get { return _Connections.AsReadOnly(); }
        }

        public Customer(string Name, decimal Balance)
        {
            _Name = Name;
            _Balance = Balance;
        }
        
        public bool ApplyCharge(decimal amount)
        {
            if (Balance > amount)
            {
                _Balance -= amount;
                return true;
            }
            else
            {
                return false;
            }
          }
        public void Register(Connection Connection)
        {
           
            if (!(_Connections.Contains(Connection)))
            {
                _Connections.Add(Connection);
            }
        }
        public override string ToString()
        {
            return string.Format("{0}:{1:C}",_Name,_Balance);
        }

    }
}
