﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication9
{
    class DeviceInventory
    {
        private Device _Device;
        private decimal _Price;

        public Device Device

        {
            get { return _Device; }
        }

         public decimal Price

        {
            get { return _Price; }
        }

        public DeviceInventory(Device device,decimal price)
        {
            _Device = device;
            _Price = price;
        }

        public override string ToString()
        {
            return string.Format("{0}: {1:C}", _Device, _Price);
        }

    }
}
