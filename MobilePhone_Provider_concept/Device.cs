﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication9
{
    class Device
    {
        //attributes
        private string _Brand;
        private string _Model;

        public string Model
        {
            get
            {
                return _Model;
            }
        }
        public string Brand
        {
            get
            {
                return _Brand;
            }
        }

            public decimal devPrice (Device device)
            {
                decimal devPrice = 0;
                foreach (DeviceInventory price in SystemDB.Phones)
                {
                    if (price.Device == device )
                    {
                        devPrice = price.Price;

                    }

                }
                return devPrice;
            }
        

        protected  Device(string Brand,string Model)
        {
            _Brand = Brand;
            _Model = Model;

        }

        public override string ToString()
        {
            return string.Format("{0}, {1}", _Brand ,_Model);
        }
    }
}
