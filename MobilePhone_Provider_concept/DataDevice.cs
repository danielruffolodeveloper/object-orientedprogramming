﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication9
{
    class DataDevice:Device
    {

        public DataDevice(string Brand,string Model): base(Brand, Model)
        {
        }
             public override string ToString()
        {
            return string.Format("{0} {1}", base.ToString(), "(Data only)");
        }
          
    }
}
