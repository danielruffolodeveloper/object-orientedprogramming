﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication9
{
    class PlanInventory
    {
        private Plans _Plans;
        private decimal _Price;

        public Plans Plan
        {
            get { return _Plans; }
        }

        public decimal Price
        {
            get { return _Price; }
        }

        public PlanInventory(Plans Plan, decimal Price)
        {
            _Plans = Plan;
            _Price = Price;
        }

        public override string ToString()
        {
            return string.Format("{0}: {1:C}", _Plans, _Price);
        }

    }
}
