﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication9
{
    class SmartPhone:Device
    {
        private string _Brand;
        private string _Model;
        public SmartPhone(string Brand,string Model): base(Brand, Model)
        {
            _Brand = Brand; 
            _Model = Model;
        }
             public override string ToString()
        {
            return string.Format("{0},{1}", base.ToString(), "(SmartPhone)");
        }
    }
}
